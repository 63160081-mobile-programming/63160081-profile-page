import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}


class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
 Widget build(BuildContext context){
   return MaterialApp(
     debugShowCheckedModeBanner: false,
     theme: currentTheme == APP_THEME.DARK
         ? MyAppTheme.appThemeLight()
         : MyAppTheme.appThemeDark(),
     home: Scaffold(
        appBar: buildAppBarWidget(),
       body: buildBodyWidget(),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.sunny),
         onPressed: (){
           setState(() {
             currentTheme == APP_THEME.DARK
                 ? currentTheme = APP_THEME.LIGHT
                 : currentTheme = APP_THEME.DARK;
           });
         },
       ),
     ),
   );
 }

 Widget buildCallButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.call,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Call"),
     ],
   );
 }

 Widget buildTextButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.chat,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Text"),
     ],
   );
 }

 Widget buildVideoButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.video_call,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Video"),
     ],
   );
 }

 Widget buildMailButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.mail,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Email"),
     ],
   );
 }

 Widget buildDirectionButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.directions,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Directions"),
     ],
   );
 }

 Widget buildPayButton() {
   return Column(
     children: <Widget>[
       IconButton(
         icon: Icon(
           Icons.attach_money,
           // color: Colors.white,
         ),
         onPressed: () {},
       ),
       Text("Pay"),
     ],
   );
 }

 //ListTile
Widget mobilePhoneListTile(){
   return ListTile(
     leading: Icon(Icons.call),
     title: Text("098-262-7881"),
     subtitle: Text("mobile"),
     trailing: IconButton(
       icon: Icon(Icons.message),
       // color: Colors.white,
       onPressed: (){},
     ),
   );
  }

 Widget otherMobilePhoneListTile(){
   return ListTile(
     leading: Text(""),
     title: Text("084-543-0222"),
     subtitle: Text("other"),
     trailing: IconButton(
       icon: Icon(Icons.message),
       // color: Colors.white,
       onPressed: (){},
     ),
   );
 }

 Widget emailListTile(){
   return ListTile(
     leading: Icon(Icons.mail),
     title: Text("63160081@go.buu.ac.th"),
     subtitle: Text("work"),
   );
 }

 Widget addressListTile(){
   return ListTile(
     leading: Icon(Icons.location_pin),
     title: Text("14/8 Lerthiruan Mansion Bangsean"),
     subtitle: Text("home"),
     trailing: IconButton(
       icon: Icon(Icons.directions),
       // color: Colors.white,
       onPressed: (){},
     ),
   );
 }

 buildAppBarWidget(){
   return AppBar(
     backgroundColor: Colors.black87,
     leading: Icon(Icons.arrow_back),
     actions: <Widget>[
       IconButton(
           onPressed: (){
             print("Contact is Starred");
           },//function
           icon: Icon(Icons.star_border))
     ],
   );
 }

 Widget buildBodyWidget(){
   return ListView (
     children: <Widget>[
       Column(
         children: <Widget>[
           Container(
             width: double.infinity,

             //Height constraint at Container widget level
             height: 250,

             child: Image.network(
               "https://yt3.ggpht.com/CNaplZm9pPcAKSXuGSlgGt09E3kBm3MC28ekrokAHHkYUqePRJ1TZiSlSzU9QelRGBFNQEuFpQ=s900-c-k-c0x00ffffff-no-rj",
               fit: BoxFit.cover,
             ),
           ),
           Container(
             height: 60,
             child: Row(
               mainAxisAlignment: MainAxisAlignment.start,
               children: <Widget>[
                 Padding(
                   padding: EdgeInsets.all(8.0),
                   child :Text("Suphakorn Auttano",
                     style: TextStyle(
                       fontSize: 24,
                     ),
                   ),
                 ),
               ],
             ),
           ),
           Divider(
             // color: Colors.black,
           ),
           Container(
             margin: const EdgeInsets.only(top: 8, bottom: 8),
               child: profileActionItems(),
   ),
           Divider(
             // color: Colors.black,
           ),
           mobilePhoneListTile(),
           otherMobilePhoneListTile(),
           Divider(
             // color: Colors.black,
           ),
           emailListTile(),
           addressListTile()
         ],
       ),
     ],
   );
 }

 Widget profileActionItems(){
   return Row(
     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
     children: <Widget>[
       buildCallButton(),
       buildTextButton(),
       buildVideoButton(),
       buildMailButton(),
       buildDirectionButton(),
       buildPayButton(),
     ],
   );
 }
}
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        dividerTheme: DividerThemeData(
          color: Colors.black,
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black87,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        dividerTheme: DividerThemeData(
          color: Colors.white,
        ),
        iconTheme: IconThemeData(
          color: Colors.pink,
        )
    );
  }
}
